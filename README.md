# VPS Configuration

Conjunto de passos para configurar uma VPS. São abordados itens como acesso, segurança, ambiente de produção, e etc.

## Sumario

* [Setup](#setup)
* [Acesso](#acesso)
* [Segurança](#segurança)

## Setup

Atualmente possuo duas VPS na [Contabo](https://my.contabo.com/vps#) com a seguinte configuração:
* 8Gb RAM
* 200Gb SSD
* 4 Cores
* Porta de 200Mb/s
* Tráfego ilimitado
* Debian 10

## Passos

### Acesso

* O primeiro passo é configurar o acesso a sua VPS, por padrão o _root_ é disponibilizado na criação, mas não é uma boa prática permanecer com esse super usuario ativo. Vamos as etapas:

  1. Após estar conectado a VPS, temos que criar um novo usuário com o comando ```adduser <username>```, onde _username_ é o nome do usuario que vai adicionar. Insira todos os dados que forem solicitados.

  2. Adicione o usuario criado ao grupo sudo: ```usermod -aG sudo <username>```.

  3. Aproveite para verificar se o _sudo_ está instalado, caso não esteja o instale com ```apt install sudo```

  3. Faça logout do servidor e reconecte com o novo usuário criado.

* Após criar o novo usuários precisamos configurar a autenticação por chave privada. Esse tipo de autenticação prove mais segurança do que usando login e senha.

  1. Primeiro geramos o conjunto de chaves na nossa maquina local como comando ```ssh-keygen -t rsa```. Será perguntado o caminho para salvar, pressione enter para manter o local padrão. É pedido que entre também com uma senha, ou enter para prosseguir sem. Usar ou não uma senha tem prós e contras, um dos motivos para usar é a segurança, pois mesmo que alguém consiga acesso ao seu computador será exigido uma senha para que acesso a VPS, o contra é justamente ter que digitar a senha a todo momento. Será necessário decidir o quanto de segurança você precisa.

  2. Agora vamos copiar a nossa chave publica para a VPS. Se estiver usando um terminal bash é possível que consiga apenas com o comando ```ssh-copy-id <username>@<ip_do_servidor>```, mas caso o comando não seja reconhecido pode fazer da seguinte forma: 
      1. Primeiro criamos a pasta _ssh_ e o arquivo _authorized_keys_ caso não existam: ```ssh <username>@<ip_do_servidor> "if [[ ! -d ~/.ssh ]]; then mkdir ~/.ssh && touch ~/.ssh/authorized_keys; fi"```

      2. Agora copiamos nossa chave usando o comando _cat_: ```cat ~/.ssh/id_rsa.pub | ssh <username>@<ip_do_servidor> "cat >> ~/.ssh/authorized_keys"```

  3. Como ultimo passo devemos desabilitar o acesso _root_ e o uso de senha.
      1. Conecte-se a VPS e abra o arquivo de configuração _ssh_ com sudo: ```sudo nano /etc/ssh/sshd_config```

      2. Altere as seguintes linhas no arquivo:
          ```
          PermitRootLogin no
          PasswordAuthentication no
          ```

      3. Finalmente, temos que reiniciar o serviço _ssh_ para que as alterações surtam efeito: ```sudo systemctl restart ssh```

### Segurança

Vou abordar dois pontos para a segurança da VPS, o fail2ban e a UFW. O fail2ban é..., já o UFW server de...

* fail2ban

#### Manter rastro de conexões SSH
>iptables -A INPUT -p tcp -m tcp --dport 7022 -m conntrack --ctstate NEW -j LOG --log-prefix "SSH connection attemption: " --log-level 1

Este comando irá monitorar sempre que alguma conexão nova tentar ser iniciada na porta SSH disparando um log no `/var/log/messages`

#### Rejeitar multiplas conexões sucessivas SSH
> iptables -A INPUT -p tcp -m tcp --dport 7022 -m conntrack --ctstate NEW -m limit --limit 5/min -j LOG --log-prefix "iptables denied: " --log-level 7

Este comando erá rejeitar multiplas conexões sucessivas em caso de tentativas de ataques à porta SSH. Além disso, caso isso ocorra, irá disparar um log no `/var/log/messages`
